#pragma comment(lib, "winmm")

#include <d2d1.h>
#include <mmsystem.h>

#include "d2d.h"
//#include "structure.h"
#include "queue.h"

#define STONE_MAX 5
#define STONE_SX 50
#define STONE_SY 50

#define SHOOT_SX 7
#define SHOOT_SY 7

#define AIRPLANE_Y 200
#define AIRPLANE_SX 40
#define AIRPLANE_SY 46

#pragma once

class game
{
private:

public:
	struct stonedata *sdata;
	int airplane_x;
	d2d *_d2d;
	game(void);
	~game(void);
	void stone(void);
	bool init(d2d *___d2d);
	bool move_airplane(int value);
	bool shoot(void);
	queue q, mq;
	bool matrix(void);
};
