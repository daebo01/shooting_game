// class create -> createrendertarget -> init -> ~

#include "StdAfx.h"
#include "d2d.h"


d2d::d2d(void)
{
	int a = 0;
	if(D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &factory) != S_OK) a = 1;
	::CoInitialize(NULL);
	if(CoCreateInstance(CLSID_WICImagingFactory, NULL, CLSCTX_INPROC_SERVER, IID_IWICImagingFactory, (LPVOID*)&wic) != S_OK) a = 1;
	if(DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(write), reinterpret_cast<IUnknown **>(&write)) != S_OK) a = 1;

	if(write->CreateTextFormat(L"Gulim", NULL, DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 10, L"", &textformat) != S_OK) a = 1;
	ASSERT(a == 0);
}

d2d::~d2d(void)
{
}

bool d2d::init(void)
{
	bitmapdata = (ID2D1Bitmap**)malloc( sizeof(ID2D1Bitmap*) * B_MAX);
	loadbitmap();

	rendertarget->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::White), &brush);
	return true;
}

ID2D1HwndRenderTarget** d2d::CreateRenderTarget(HWND _hWnd)
{
	RECT size;
	hWnd = _hWnd;
	GetClientRect(hWnd,&size);
	D2D1_SIZE_U dsize = D2D1::SizeU(size.right - size.left, size.bottom - size.top);
	if(factory->CreateHwndRenderTarget(D2D1::RenderTargetProperties(), D2D1::HwndRenderTargetProperties(hWnd, dsize), &rendertarget) != S_OK ) return NULL;

	return &rendertarget;
}

void d2d::paint(void)
{
	rendertarget->BeginDraw();
	rendertarget->Clear(D2D1::ColorF(D2D1::ColorF::Blue));
	//ID2D1SolidColorBrush *brush;
	//GetClientRect(hWnd, &size);
	//rendertarget->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Red), &brush);

	//rendertarget->DrawRectangle(D2D1::RectF(size.left + 100.0f, size.top + 100.0f, size.right - 100.0f, size.bottom - 100.0f), brush);
	
	//D2D1_RECT_F area = ::D2D1::RectF(0.0f,0.0f,50.0f,50.0f);
	//rendertarget->DrawBitmap(bitmap, area);


	rendertarget->EndDraw();
}

bool d2d::loadbitmap(wchar_t name[20], ID2D1Bitmap **bitmap)
{
	IWICFormatConverter *wicconv;
	IWICBitmapDecoder *wicdecoder;
	IWICBitmapFrameDecode *wicframe;

	wic->CreateDecoderFromFilename(name, NULL, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &wicdecoder);
	wicdecoder->GetFrame(0, &wicframe);
	wic->CreateFormatConverter(&wicconv);
	wicconv->Initialize(wicframe, GUID_WICPixelFormat32bppPBGRA, WICBitmapDitherTypeNone, NULL, 0.0f, WICBitmapPaletteTypeCustom);

	ASSERT((*bitmap) != NULL);
	rendertarget->CreateBitmapFromWicBitmap(wicconv, NULL, bitmap);

	wicdecoder->Release();
	wicframe->Release();
	wicconv->Release();
	return false;
}

bool d2d::loadbitmap(void)
{
	loadbitmap(L"stone.bmp", &(bitmapdata[B_STONE]));
	loadbitmap(L"shoot.bmp", &(bitmapdata[B_SHOOT]));
	loadbitmap(L"airplane.bmp", &(bitmapdata[B_AIRPLANE]));
	
	return false;
}

bool d2d::drawtext(void)
{

	return false;
}

int d2d::draw1(float x, float y)
{
	rendertarget->DrawTextW(L"*", wcslen(L"*"), textformat, D2D1::RectF(x, y, x + 10.0f, y + 10.0f), brush);
	return 0;
}