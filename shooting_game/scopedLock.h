#pragma once

#include <mutex>

class scopedLock {
	public:
		scopedLock() = delete;
		scopedLock(const scopedLock&) = delete;
		scopedLock& operator=(const scopedLock&) = delete;

	public:
		scopedLock(std::mutex &lock);
		~scopedLock();

	private:
		std::mutex &lock;
};