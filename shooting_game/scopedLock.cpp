#include "stdafx.h"

#include "scopedLock.h"

scopedLock::scopedLock(std::mutex &lock) : lock(lock) {
	lock.lock();
}

scopedLock::~scopedLock() {
	lock.unlock();
}