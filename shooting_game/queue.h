#include <atomic>
#include <mutex>

#include "structure.h"

class queue {
public:
	queue();
	~queue();
	void add(size_t*);
	size_t* remove();
	struct page *start, *end, *now;
	std::atomic<int> count;
	//int lock;
	std::mutex lock;

private:
	
};