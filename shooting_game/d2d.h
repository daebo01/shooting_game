#pragma once
#pragma comment(lib, "d2d1.lib")
#pragma comment(lib, "WindowsCodecs.lib")
#pragma comment(lib, "dwrite.lib")

#include <d2d1.h>
#include <d2d1helper.h>
#include <dwrite.h>
#include <wincodec.h>

#include "resource.h"


class d2d
{
public:
	d2d(void);
	~d2d(void);
	bool init(void);
private:
	ID2D1Factory *factory;
	HWND hWnd;
	IWICImagingFactory *wic;
	IDWriteFactory *write;
	IDWriteTextFormat *textformat;
	ID2D1SolidColorBrush *brush;

public:
	ID2D1HwndRenderTarget** CreateRenderTarget(HWND hWnd);
	void paint(void);
	bool loadbitmap(wchar_t name[20], ID2D1Bitmap **bitmap);
	bool loadbitmap(void);
	ID2D1HwndRenderTarget *rendertarget;
	ID2D1Bitmap **bitmapdata;
	bool drawtext(void);
	int draw1(float x, float y);
	bool dwrite_init(void);
};
