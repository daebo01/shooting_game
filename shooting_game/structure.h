#include <mutex>

struct page {
	size_t* a;
	struct page* next;
};

struct airplanedata {
	int x, y;
	size_t *a;
};

struct stoneinfo {
	int x, y, use, rad;
};

struct stonedata {
	//int cur;
	struct stoneinfo **sinfo;
	std::mutex lock;
};