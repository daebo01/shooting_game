#include "StdAfx.h"

#include "scopedLock.h"

#include "game.h"

game::game()
{
	airplane_x = 0;

	srand(static_cast<unsigned int>(time(NULL)) + GetCurrentThreadId());
}

game::~game(void)
{
}

UINT repaint(LPVOID a) {
	game *_game = (game*)a;
	int i;

	D2D1_RECT_F area;
	D2D1::Matrix3x2F rot;
	D2D1::Matrix3x2F trans; 
	Sleep(100);
	
	while(1) {
		_game->_d2d->rendertarget->BeginDraw();
		_game->_d2d->rendertarget->Clear(D2D1::ColorF(D2D1::ColorF::Black));

		{
			// draw matrix
			const auto &&slock = scopedLock(_game->mq.lock);
			int i = 0;

			_game->mq.now = _game->mq.start;
			while (_game->mq.now != NULL) {
				for (int j = 0; j < 5; j++) _game->_d2d->draw1(((float*)_game->mq.now->a)[j], static_cast<float>(1 + 15 * i));
				if (_game->mq.now->next == NULL) break;
				_game->mq.now = _game->mq.now->next;
				i++;
			}
		}
			

		for(i=0;i<STONE_MAX;i++) {
			if(_game->sdata->sinfo[i]->use == 1) {
				area = ::D2D1::RectF((float)_game->sdata->sinfo[i]->x, (float)_game->sdata->sinfo[i]->y, (float)_game->sdata->sinfo[i]->x + STONE_SX, (float)_game->sdata->sinfo[i]->y +STONE_SY);
				_game->_d2d->rendertarget->DrawBitmap(_game->_d2d->bitmapdata[B_STONE], area);

				rot = ::D2D1::Matrix3x2F::Rotation(0.0f, D2D1::Point2F(0.0f, 0.0f));
				trans = ::D2D1::Matrix3x2F::Translation(0.0f, 0.0f); 
				_game->_d2d->rendertarget->SetTransform(rot * trans);
			}
			else if(_game->sdata->sinfo[i]->use == 2) {
				area = ::D2D1::RectF(0.0f, 0.0f, STONE_SX, STONE_SY);
				rot = ::D2D1::Matrix3x2F::Rotation((float)45-_game->sdata->sinfo[i]->rad, D2D1::Point2F(STONE_SX/2, STONE_SY/2) );
				trans = ::D2D1::Matrix3x2F::Translation((float)_game->sdata->sinfo[i]->x, (float)_game->sdata->sinfo[i]->y); 
 				_game->_d2d->rendertarget->SetTransform(rot * trans);
				_game->_d2d->rendertarget->DrawBitmap(_game->_d2d->bitmapdata[B_STONE], area, (float)(_game->sdata->sinfo[i]->rad*2.0/100.0));

				rot = ::D2D1::Matrix3x2F::Rotation(0.0f, D2D1::Point2F(0.0f, 0.0f));
				trans = ::D2D1::Matrix3x2F::Translation(0.0f, 0.0f); 
				_game->_d2d->rendertarget->SetTransform(rot * trans);

				_game->sdata->sinfo[i]->rad -= 5;
				if(_game->sdata->sinfo[i]->rad <= 0) _game->sdata->sinfo[i]->use = 0;
			
			}
		}

		_game->q.now = _game->q.start;
		while(_game->q.now != NULL) {
			area = ::D2D1::RectF((float)((struct airplanedata*)_game->q.now->a)->x,(float)((struct airplanedata*)_game->q.now->a)->y,(float)((struct airplanedata*)_game->q.now->a)->x + SHOOT_SX,(float)((struct airplanedata*)_game->q.now->a)->y + SHOOT_SY);
			_game->_d2d->rendertarget->DrawBitmap(_game->_d2d->bitmapdata[B_SHOOT], area);
			if(_game->q.now->next == NULL) break;
			_game->q.now = _game->q.now->next;
		}
		
		area = ::D2D1::RectF((float)_game->airplane_x,(float)AIRPLANE_Y ,(float)_game->airplane_x + AIRPLANE_SX,(float)AIRPLANE_Y + AIRPLANE_SY);
		_game->_d2d->rendertarget->DrawBitmap(_game->_d2d->bitmapdata[B_AIRPLANE], area);
			
		
		_game->_d2d->rendertarget->EndDraw();
		Sleep(20);
	}
	return 0;
}

UINT stone_new(LPVOID a) {
	Sleep(1000);
	game *_game = (game*)a;
	int i = 0;
	
	while(1) {
		bool stonePlace = false;

		{
			const auto &&slock = scopedLock(_game->sdata->lock);
			
			if(_game->sdata->sinfo[i]->use == 0) {
				_game->sdata->sinfo[i]->x = (int)((double)rand() / RAND_MAX * (560 - STONE_SX));
				_game->sdata->sinfo[i]->y = 0;
				_game->sdata->sinfo[i]->use = 1;
				stonePlace = true;
			}
		}

		if (stonePlace) {
			Sleep(1000);
		}
	
		Sleep(500);
		i++;
		if(i == STONE_MAX) i = 0;

	}


	return 0;
}

UINT stone_thread(LPVOID a) {
	Sleep(1000);
	game *_game = (game*)a;
	int i = 0;
	
	while(1) {
		
		{
			const auto &&slock = scopedLock(_game->sdata->lock);

			if(_game->sdata->sinfo[i]->use == 1) {
				if(_game->sdata->sinfo[i]->y <= 200 - STONE_SY - 10) {
					_game->sdata->sinfo[i]->y += 10;
				}
				else {
					_game->sdata->sinfo[i]->use = 0;
				}

			}
		}

		Sleep(200);
		i++;
		if(i == STONE_MAX) i = 0;

	}

	return 0;
}

void game::stone(void)
{
	int i;

	sdata = new struct stonedata;
	sdata->sinfo = new struct stoneinfo*[STONE_MAX];
	for(i=0;i<STONE_MAX;i++) {
		sdata->sinfo[i] = new struct stoneinfo;
		sdata->sinfo[i]->use = 0;
	}
	
	AfxBeginThread(stone_new, this);
	AfxBeginThread(stone_thread, this);
	AfxBeginThread(repaint, this);
	return;
}


bool game::init(d2d *___d2d)
{
	_d2d = ___d2d;
	
	stone();
	matrix();
	return false;
}

bool game::move_airplane(int value)
{
	airplane_x += value;

	if(airplane_x > 560 - AIRPLANE_SX) airplane_x = 560 - AIRPLANE_SX;
	if(airplane_x < 0) airplane_x = 0;

	return true;
}


UINT shoot_thread(LPVOID a) {
	int i, j, y;
	struct airplanedata *_data = (airplanedata*)a;

	game *_game = (game*)_data->a;


	//i= _data->y - 10;
	y = _data->y;
	for(i=y;i>10;i-=9) {
		for(j=0;j<STONE_MAX;j++) {
			if(_game->sdata->sinfo[j]->use == 1 && _data->x > _game->sdata->sinfo[j]->x && _data->x < _game->sdata->sinfo[j]->x + STONE_SX && i < _game->sdata->sinfo[j]->y + STONE_SY) {
				PlaySound(L"stone.wav", AfxGetInstanceHandle(),  SND_ASYNC);
				_game->sdata->sinfo[j]->use = 2;
				_game->sdata->sinfo[j]->rad = 45;
				i = -100;
				break;
			}
		}
		_data->y = i;
		Sleep(100);
	}
	
	_game->q.remove();
	free((airplanedata*)a);
	return 0;
}



bool game::shoot(void) {
	struct airplanedata *_data;

	_data = new struct airplanedata;

	_data->x = airplane_x + (AIRPLANE_SX/2) - (SHOOT_SX/2);
	_data->y = AIRPLANE_Y;
	_data->a = (size_t*)this;
	
	AfxBeginThread(shoot_thread, _data);
	q.add((size_t*)_data);
	PlaySound(L"shoot.wav", AfxGetInstanceHandle(),  SND_ASYNC); // �л�
	//CloseHandle(p);
	
	
	return false;
}

UINT matrix_thread(LPVOID a) {
	int i;
	//float x;
	game *_game = (game*)a;
	
	while(1) {
		float *f = new float[5];
		for(i=0;i<5;i++) f[i] = (float)((double)rand() / RAND_MAX * 560.0f);

		if(_game->mq.count >= 15) {
			size_t *af;
			af = _game->mq.remove();
			delete[] (float*)af;
		}

		_game->mq.add((size_t*)f);


		Sleep(100);
	}
	return 0;
}

bool game::matrix(void)
{
	AfxBeginThread(matrix_thread, this);
	return false;
}
