#include "stdafx.h"

#include "scopedLock.h"

#include "queue.h"

queue::queue() {
	count = 0;
	start = NULL;
	end = NULL;
}

queue::~queue() {
}

void queue::add(size_t *data) {
	const auto &&slock = scopedLock(lock);

	page* temp;
	temp = new page;
	temp->a = data;
	temp->next = NULL;
	if(start == NULL) {
		start = temp;
		end = temp;
	}
	else {
		end->next = temp;
		end = temp;
	}
	count++;

	return;
}

size_t* queue::remove() {
	const auto &&slock = scopedLock(lock);

	page* temp = start;
	size_t* a = temp->a;
	start = temp->next;
	delete temp;
	//temp = NULL;
	--count;

	return a;
}